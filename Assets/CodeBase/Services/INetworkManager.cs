﻿using System;
using CodeBase.Infrastructure.Services;
using UnityEngine;

namespace CodeBase.Services
{
    public interface INetworkManager : IService
    {
        void StartClient(string playerName,Action clientReady);
    }
}