﻿using System;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CodeBase.Services
{
    public class NetworkManagerService : MonoBehaviourPunCallbacks, INetworkManager
    {
        #region Events

        public event Action ClientReady;

        #endregion

        public void StartClient(string playerName,Action clientReady)
        {
            ClientReady = clientReady;

            if (!PhotonNetwork.IsConnectedAndReady)
            {
                PhotonNetwork.NickName =string.IsNullOrEmpty(playerName)? $"PLayer {Random.Range(0, 15)}" : playerName;
                PhotonNetwork.ConnectUsingSettings();
            }
            else
            {
                PhotonNetwork.JoinLobby();
            }

        }

        #region Photon Callback Methods
        
        public override void OnConnectedToMaster()
        {
            PhotonNetwork.JoinLobby();
        }

        public override void OnJoinedLobby()
        {
            CreateAndJoinRoom();
        }

        public override void OnJoinedRoom()
        {
            ClientReady?.Invoke();
        }

        public override void OnCreatedRoom()
        {
        }

        #endregion

        #region Private Methods

        private void CreateAndJoinRoom()
        {
            string randomRoomName = $"Room{Random.Range(0, 20)}";
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsOpen = true;
            roomOptions.IsVisible = true;
            roomOptions.MaxPlayers = 15;

            PhotonNetwork.JoinRandomOrCreateRoom(null,15,MatchmakingMode.FillRoom,null,null,randomRoomName,roomOptions);
        }

        #endregion
        
    }
}