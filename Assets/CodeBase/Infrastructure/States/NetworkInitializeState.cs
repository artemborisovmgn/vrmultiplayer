﻿using CodeBase.Services;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class NetworkInitializeState : IState
    {
        private const string LevelName = "AreaScene";
        private readonly GameStateMachine _stateMachine;
        private readonly INetworkManager _networkManager;
        private readonly string _playerName;

        public NetworkInitializeState(GameStateMachine stateMachine, INetworkManager networkManager, string playerName)
        {
            _stateMachine = stateMachine;
            _networkManager = networkManager;
            _playerName = playerName;
        }
        
        public void Enter()
        {
            _networkManager.StartClient(_playerName,OnClientReady);
        }

        private void OnClientReady()
        {
            _stateMachine.Enter<LoadLevelState,string>(LevelName);
        }

        public void Exit()
        {
            
        }
    }
}