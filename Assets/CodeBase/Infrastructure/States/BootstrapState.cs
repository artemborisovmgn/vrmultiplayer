﻿using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class BootstrapState : IState
    {
        #region PrivateVariables
        private readonly GameStateMachine _stateMachine;
        private readonly AllServices _services;

        #endregion
        
        public BootstrapState(GameStateMachine stateMachine, AllServices services)
        {
            _stateMachine = stateMachine;
            _services = services;
        }

        public void Enter()
        {
            RegisterServices();
            
        }

        public void Exit() { }

        private void RegisterServices()
        {
            _services.RegisterSingle<IAssets>(new PhotonAssetProvider());
            _services.RegisterSingle<IFactory>(new GameFactory(_services.Single<IAssets>()));
            _stateMachine.Enter<NetworkInitializeState>();
        }
    }
}