﻿using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using CodeBase.Services;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class GenerateAssetsState : IState
    {
        private const string Initialpoint = "InitialPoint";
        private readonly GameStateMachine _stateMachine;
        private readonly AllServices _services;
        private readonly GameObject _playerPrefab;


        public GenerateAssetsState(GameStateMachine stateMachine, AllServices services, GameObject playerPrefab)
        {
            _stateMachine = stateMachine;
            _services = services;
            _playerPrefab = playerPrefab;
        }

        public void Enter()
        {
            var gameFactory = _services.Single<IFactory>();
            var initialPoint = GameObject.FindWithTag(Initialpoint);
            gameFactory.CreatePlayer(_playerPrefab, initialPoint.transform);
        }

        public void Exit()
        {
            
        }
    }
}