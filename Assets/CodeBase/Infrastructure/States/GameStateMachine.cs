﻿using System;
using System.Collections.Generic;
using CodeBase.Infrastructure.Services;
using CodeBase.Services;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class GameStateMachine
    {
        private readonly Dictionary<Type, IExitableState> _states;
        private IExitableState _activeState;

        public GameStateMachine(SceneLoader loader,NetworkManagerService manager,AllServices services, GameObject playerPrefab,string playerName)
        {
            _states = new()
            {
                [typeof(BootstrapState)] = new BootstrapState(this,services),
                [typeof(NetworkInitializeState)] = new NetworkInitializeState(this,manager,playerName),
                [typeof(LoadLevelState)] = new LoadLevelState(this,loader),
                [typeof(GenerateAssetsState)] = new GenerateAssetsState(this,services,playerPrefab),
            };
        }
        
        public void Enter<TState>() where TState : class, IState
        {
            var state = ChangeState<TState>();
            state.Enter();
        }
        
        public void Enter<TState, TPayLoad>( TPayLoad payLoad) where TState : class, IPayloadedState<TPayLoad>
        {
            var state = ChangeState<TState>();
            state.Enter(payLoad);
        }

        private TState ChangeState<TState>() where TState : class, IExitableState
        {
            _activeState?.Exit();
            
            TState state = GetState<TState>();
            _activeState = state;
            
            return state;
        }
        private TState GetState<TState>() where TState : class, IExitableState =>
            _states[typeof(TState)] as TState;
    }
}