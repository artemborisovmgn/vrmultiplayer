﻿using CodeBase.Infrastructure.Services;
using UnityEngine;

namespace CodeBase.Infrastructure.Factory
{
    public interface IFactory : IService
    {
        GameObject PlayerInstance { get; set; }
        GameObject CreatePlayer(GameObject player, Transform at);
    }
}