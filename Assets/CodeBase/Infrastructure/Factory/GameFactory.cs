﻿using CodeBase.Infrastructure.AssetManagement;
using UnityEngine;

namespace CodeBase.Infrastructure.Factory
{
    public class GameFactory : IFactory
    {
        private IAssets _assets;

        public GameObject PlayerInstance { get; set; }

        public GameFactory(IAssets assets)
        {
            _assets = assets;
        }

        public GameObject CreatePlayer(GameObject player, Transform at)
        {
            PlayerInstance =_assets.Instantiate(player, at.position, at.rotation);
             return PlayerInstance;
        }
    }
}