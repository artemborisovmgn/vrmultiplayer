﻿using CodeBase.Infrastructure.Services;
using CodeBase.Infrastructure.States;
using CodeBase.Services;
using UnityEngine;
using TMPro;

namespace CodeBase.Infrastructure
{
    public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
    {
        #region Inspector

        [SerializeField] private GameObject playerPrefab;
        [SerializeField] private NetworkManagerService manager;
        [SerializeField] private TMP_InputField _inputField;
        #endregion

        public GameStateMachine StateMachine { get; private set; }

        private void Start()
        {
            DontDestroyOnLoad(this);
        }
        
        public void InitializeGame()
        {
            StateMachine = new GameStateMachine(new SceneLoader(this) ,manager, AllServices.Container, playerPrefab,_inputField.text);

            StateMachine.Enter<BootstrapState>();
        }
    }
}