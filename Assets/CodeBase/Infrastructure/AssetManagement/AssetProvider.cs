﻿using UnityEngine;

namespace CodeBase.Infrastructure.AssetManagement
{
    public class AssetProvider : IAssets
    {
        public GameObject Instantiate(GameObject obj)
        {
            var spawnObject = Object.Instantiate(obj);
            return spawnObject;
        }

        public GameObject Instantiate(GameObject obj, Vector3 at)
        {
            var spawnObject = Object.Instantiate(obj);
            spawnObject.transform.position = at;
            
            return spawnObject;
        }

        public GameObject Instantiate(GameObject obj, Vector3 at, Quaternion rotation)
        {
            var spawnObject = Object.Instantiate(obj);
            spawnObject.transform.position = at;
            spawnObject.transform.rotation = rotation;

            return spawnObject;
        }
    }
}