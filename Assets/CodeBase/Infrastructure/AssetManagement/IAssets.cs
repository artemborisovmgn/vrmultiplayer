﻿using CodeBase.Infrastructure.Services;
using UnityEngine;

namespace CodeBase.Infrastructure.AssetManagement
{
    public interface IAssets : IService
    {
        GameObject Instantiate(GameObject obj);
        GameObject Instantiate(GameObject obj, Vector3 at);
        GameObject Instantiate(GameObject obj, Vector3 at, Quaternion rotation);
    }
}