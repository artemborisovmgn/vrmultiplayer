﻿using Photon.Pun;
using UnityEngine;

namespace CodeBase.Infrastructure.AssetManagement
{
    public class PhotonAssetProvider : IAssets
    {
        public GameObject Instantiate(GameObject obj)
        {
            GameObject instanceObl = null;
            if (PhotonNetwork.IsConnectedAndReady)
            {
                instanceObl = PhotonNetwork.Instantiate(obj.name,Vector3.zero, Quaternion.identity);
            }

            return instanceObl;
        }

        public GameObject Instantiate(GameObject obj, Vector3 at)
        {
            GameObject instanceObl = null;
            if (PhotonNetwork.IsConnectedAndReady)
            {
                instanceObl = PhotonNetwork.Instantiate(obj.name,at,Quaternion.identity,0);
            }
            Debug.Log("roomObject");
            return instanceObl;
        }

        public GameObject Instantiate(GameObject obj, Vector3 at, Quaternion rotation)
        {
            GameObject instanceObl = null;
            if (PhotonNetwork.IsConnectedAndReady)
            {
                instanceObl = PhotonNetwork.Instantiate(obj.name,at,rotation);
            }

            return instanceObl;
        }
    }
}