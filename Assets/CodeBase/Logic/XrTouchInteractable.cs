using System;
using UnityEngine.XR.Interaction.Toolkit;

namespace CodeBase.Logic
{
    public class XrTouchInteractable : XRBaseInteractable
    {
        #region Events

        public event Action ItemSelect;
        public event Action HoverEnter;
        public event Action HoverExit;

        #endregion

        #region Callbacks
        protected override void OnSelectEntered(SelectEnterEventArgs args)
        {
            ItemSelect?.Invoke();
        }

        protected override void OnHoverEntered(HoverEnterEventArgs args)
        {
            HoverEnter?.Invoke();
        }

        protected override void OnHoverExited(HoverExitEventArgs args)
        {
            HoverExit?.Invoke();
        }
        #endregion
    }
}
