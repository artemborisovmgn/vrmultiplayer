﻿using TMPro;
using UnityEngine;

namespace CodeBase.Logic.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class ViewPanel : IViewPanel
    {
        #region Inspector

        [SerializeField]private TMP_Text _playerNameText;

        #endregion
        #region Private Variables

        private CanvasGroup CanvasGroup;
        
        #endregion

        private void Start()
        {
            CanvasGroup = GetComponent<CanvasGroup>();
            HidePanel();
        }

        public override void ShowPanel()
        {
            CanvasGroup.alpha = 1;
            CanvasGroup.interactable = true;
            CanvasGroup.blocksRaycasts = true;
        }

        public override void SetInfo(string info)
        {
            _playerNameText.text = info;
        }

        public override void HidePanel()
        {
            CanvasGroup.alpha = 0;
            CanvasGroup.interactable = false;
            CanvasGroup.blocksRaycasts = false;
        }
    }
}