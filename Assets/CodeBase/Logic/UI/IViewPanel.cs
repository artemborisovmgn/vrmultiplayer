﻿using UnityEngine;

namespace CodeBase.Logic.UI
{
    public abstract class IViewPanel : MonoBehaviour
    {
        public abstract void ShowPanel();

        public abstract void SetInfo(string info);
        public abstract void HidePanel();
    }
}