﻿using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using CodeBase.Logic.UI;
using CodeBase.Networking;
using Photon.Pun;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CodeBase.Logic
{
    public class NetworkChair : MonoBehaviourPunCallbacks
    {
        #region Inspector
        [SerializeField]
        [Tooltip("Action When invoke diselect event")]
        private InputActionProperty _diselectMoveAction;
        
        [SerializeField]
        [Tooltip("Action When invoke diselect event")]
        private InputActionProperty _diselectTeleportAction;
        
        [SerializeField] private XrTouchInteractable touchInteractable;
        [SerializeField] private IViewPanel _viewPanel;
        #endregion

        #region Private Variables
        private int currentSitingPlayerID;
        private bool isBusy;
        
        #endregion

        #region UnityMethods
        private void Start()
        {
            touchInteractable.ItemSelect += OnItemSelect;
            touchInteractable.HoverEnter += OnHoverEnter;
            touchInteractable.HoverExit += OnHoverExit;
        }
        
        public override void OnDisable()
        {
            touchInteractable.ItemSelect -= OnItemSelect;
            touchInteractable.HoverEnter -= OnHoverEnter;
            touchInteractable.HoverExit -= OnHoverExit;

            if (PhotonNetwork.LocalPlayer.ActorNumber == currentSitingPlayerID && isBusy)
            {
                photonView.RPC(nameof(PlayerStandUpOnChair),RpcTarget.AllBuffered);
            }
        }
        #endregion

        #region Callbacks
        
        private void OnHoverExit()
        {
            _viewPanel.HidePanel();
        }

        private void OnHoverEnter()
        {
            if(isBusy)
                _viewPanel.ShowPanel();
        }
        
        private void OnItemDeselect()
        {
            photonView.RPC(nameof(PlayerStandUpOnChair),RpcTarget.AllBuffered);
        }

        private void OnItemSelect()
        {
            if (isBusy) return;
            
            var factory = AllServices.Container.Single<IFactory>();
            
            if (factory == null || factory.PlayerInstance == null) return;
            GameObject SitObj = null;
             
            if (!PhotonNetwork.LocalPlayer.IsLocal)
            {
                SitObj = factory.PlayerInstance.GetComponent<MultiplayerVRSynchronization>().mainAvatarTransform.gameObject;
            }
            else
            {
                SitObj = factory.PlayerInstance.GetComponent<PlayerNetworkSetup>().LocalXRRigGameobject;
            }

            SitObj.transform.position = transform.position;
            SitObj.transform.rotation = Quaternion.Euler(transform.eulerAngles);
            
            currentSitingPlayerID = PhotonNetwork.LocalPlayer.ActorNumber;
            
            _diselectMoveAction.action.performed += OnDiselectPerfomed;
            _diselectTeleportAction.action.performed += OnDiselectPerfomed;
            
            photonView.RPC(nameof(SetPlayerOnChair),RpcTarget.AllBuffered,PhotonNetwork.LocalPlayer.NickName);
        }
        
        private void OnDiselectPerfomed(InputAction.CallbackContext obj)
        {
            if (PhotonNetwork.LocalPlayer.ActorNumber == currentSitingPlayerID)
            {
                OnItemDeselect();
                
                _diselectMoveAction.action.performed -= OnDiselectPerfomed;
                _diselectTeleportAction.action.performed -= OnDiselectPerfomed;
            }
        }

        #endregion

        #region NetworkMethods
        
        [PunRPC]
        private void SetPlayerOnChair(string playerName)
        {
            _viewPanel.SetInfo(playerName);
            isBusy = true;
        }
        
        [PunRPC]
        private void PlayerStandUpOnChair()
        {
            isBusy = false;

            currentSitingPlayerID = 0;
            _viewPanel.HidePanel();
        }
        
        #endregion
    }
}